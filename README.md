### Opis funkcji komponentu CMA
Komponent odpowiedzialny za zarządzanie użytkownikami, ich rolami i uprawnieniami. Pełni również funkcję frontendu do zarządzania słownikami z komponentu seup-dict.

### Użyte technologie
- Java EE 7
- JSF 2.2.14
- Primefaces 6.0
- Apache CXF 3.0.8
- Liquibase 3.1.1
- Hibernate 4.3.11.Final
- Postgres 9.5
- Apache Tomcat 9 
- Junit 4.11
- Mockito 1.9.5

### Diagram komponentu 
```plantuml
@startuml c4
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Component.puml

Person(user, "Użytkownik")
ContainerDb(db, "Postgres", "Baza danych")
System_Ext(integrator, "Integrator")
Container(seupRpt, "seup-rpt", "Kontener z JasperServer")
Container(seupEsb, "seup-esb", "Kontener z WSO2")
Container(seupSeod, "seup-seod", "SEOD")
Container(seupDict, "seup-dict", "DICT")
Container(seupEventLog, "seup-event-log", "EVENT_LOG")
Container(seupWww, "seup-www", "WWW")
Container(seupCma, "seup-ma", "CMA")

Rel(seupCma, db, "Zapis i odczyt", "JDBC")
Rel(user, seupCma, "Używa")
Rel(seupCma, seupEsb, "Używa WS", "SOAP")
Rel(seupEsb, integrator, "Używa WS", "SOAP")
Rel(seupCma, seupRpt, "Używa REST API", "JSON/HTTP")
Rel(seupCma, seupSeod, "Używa REST API", "JSON/HTTP")
Rel(seupCma, seupDict, "Używa REST API", "JSON/HTTP")
Rel(seupCma, seupEventLog, "Używa REST API", "JSON/HTTP")
Rel(seupWww, seupCma, "Używa REST API", "JSON/HTTP")
Rel(seupSeod, seupCma, "Używa REST API", "JSON/HTTP")

@enduml
```
### Instrukcja budowania i uruchomienia komponentu
#### Przed zbudowaniem aplikacji, należy wczesniej zbudować i umieścić w repozytorium lokalnym m2 projekty:
 - seup-parent-pom
 - seup-cannonical-model
 - seup
 - seup-dict

#### Procedura instalacji na lokalnym środowisku - należy:
1. Utworzyć schemat na bazie -> seup_ma
2. Utworzyć schemat na bazie -> integration
3. Utworzyć usera ma (hasło: ma) na bazie i dopisać do seup_ma i integration
4. Wgrać do /tomcat/libs pliki:
    - activation-1.1.1.jar
    - mail-1.5.0-b01.jar
    - postgresql-42.0.0.jre7.jar

5. Skonfigurować wysyłkę e-mail w context.xml:
    ```xml
     <Resource  name="mail/Session" auth="Container" type="javax.mail.Session" mail.smtp.debug="true" mail.smtp.host="smtp.mailtrap.io" mail.smtp.port="465" mail.smtp.auth="true" mail.smtp.ssl="true"
                mail.smtp.socketFactory.port="465" mail.smtp.socketFactory.class="javax.net.ssl.SSLSocketFactory"
                mail.smtp.user="a78dac8a2fe20d" mail.smtp.password="24af38c8b6dc7b" username="a78dac8a2fe20d" password="24af38c8b6dc7b" mail.debug="false" mail.smtp.from="default@seup" />
    ```

6. Skonfigurować połączenie do bazy danych w server.xml
    ```xml
           <Resource 
             name="jdbc/seup_ma" 
             auth="Container" 
             type="javax.sql.DataSource"
             maxActive="20" 
             maxIdle="5" 
             maxWait="10000" 
             username="ma" 
             password="ma" 
             validationQuery="select 1"
             driverClassName="org.postgresql.Driver" 
             logAbandoned="true" 
             removeAbandoned="true"
             url="jdbc:postgresql://localhost:5432/seup">
            </Resource>
    ```

7. Utworzyć katalogi:
    - /tomcat/logs/arch - na potrzeby archiwizacji logów
    - /opt/seup/cma/config - przenieść konfigurację z katalogu umk-seup-ma/deployment/config/local
    - /opt/seup/cma/data/cache - na potrzeby plików tymczaswoych Ehcache
8. Zbudować aplikacje poleceniem `mvn install`
9. Umieścić zbudowany plik `ma-app-web/target/cma.war` na serwerze tomcat.
8. Aby uruchomić aplikację, musimy zostać poprawnie zautoryzowani. Ponieważ na środowiskach logowaniem zajmuje sie PROXY (NGINX), w przypadku środowiska deweloperskiego (lokalnego) musimy zamodelować to zachowanie.
Centralny Moduł Administracyjny oczekuje nagłówka autoryzacyjnego w każdym request.

   Najłatwiejszym sposobem na osiągnięcie tego efektu jest instalacja dodatku do przeglądarki internetowej, np. ModifyHeaders i ustawiając nagłówek o nazwie Authorization i postaci:

    ```
    CAS token="36bef7f8-fa97-0c73-e177-65ee260adcab", kontoId="1f07c0b5-662e-8e02-caa4-bc37c50892ff", kontekstId="d999577b-9524-3b72-687d-5a971d30ad09"
    ```
   należy podmienić wartości parametrów kontoId oraz kontekstId zgodnie z danymi w lokalnej bazie danych (odpowiednio seup_ma.user_account.uuid oraz seup_ma.work_context.uuid).  

### Testowanie aplikacji
W głównym folderze aplikacji należy uruchomić polecenie `mvn test`.

### Zależności zewnętrzne 
- baza danych Postgres 9.5 ze schematami seup_ma i integration
- system Integrator (przes szyne WSO2 `seup-esb`)
- serwer JasperReports `seup-rpt` (generowanie raportów)
- `seup-dict`(słowniki)
- `seup-event-log` (dziennik zdarzeń)
- `seup-seod` (liczenie spraw przypisanych do jednostki org)

### Pobieranie danych użytkowników z systemu Integrator
- pobieranie odbywa się poprzez szyne wso2 `seup-esb`
- pobrane dane zapisywane są w schemacie `integration`
- następnie przenoszone są do schematu `seup_ma`

### Api 
swagger (dostęp po zalogowaniu):
 - środowisko alfa: https://seod-alfa.gmk.local/uslugi/cma/swagger
 - środowisko beta: https://seod-beta.gmk.local/uslugi/cma/swagger

